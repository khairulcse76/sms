<div class="modal fade" tabindex="-1" role="dialog" id="group-show">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Create New Groups
            </div>
            <form action="{{URL('authorize/insert-group')}}" method="POST" id="frm-group-create">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="group" class="control-label">Batch:</label>
                        <input name="group" class="form-control" id="group" placeholder="Enter group Name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" data-dismiss="">Create Group</button>
                </div>
            </form>
        </div>
    </div>
</div>
