<div class="modal fade" tabindex="-1" role="dialog" id="time-show">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Set a New time
            </div>
            <form action="{{URL('authorize/insert-time')}}" method="POST" id="frm-time-create">
                {{ csrf_field() }}
                <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="time" class="control-label">Start Time:</label>
                                    <div class="input-group">
                                        <input name="start_time" class="form-control" id="start_time" placeholder="Example:- 11:30 AM To" required>
                                        <div class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="time" class="control-label">End Time:</label>
                                    <div class="input-group">
                                        <input name="end_time" class="form-control" id="end_time" placeholder="Example:- 11:30 AM To" required>
                                        <div class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" data-dismiss="">Create Time</button>
                </div>
            </form>
        </div>
    </div>
</div>
