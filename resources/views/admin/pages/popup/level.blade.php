
    <div class="modal fade" tabindex="-1" role="dialog" id="level-show">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <form action="{{URL('authorize/insert-level')}}" method="POST" id="frm-level-create">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="program_id" class="control-label">Program Id:</label>
                            <select name="program_id" class="form-control" id="program_id" placeholder="Enter program id"></select>
                        </div>
                        <div class="form-group">
                            <label for="level" class="control-label">Level:</label>
                            <input name="level" class="form-control" id="level" placeholder="Enter Level" required>
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Description</label>
                            <textarea name="description" id="description" placeholder=" description" class="form-control" cols="12"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
