<div class="modal fade" tabindex="-1" role="dialog" id="shift-show">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Insert Shift
            </div>
            <form action="{{URL('authorize/insert-shift')}}" method="POST" id="frm-shift-create">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="level" class="control-label">Level:</label>
                        <input name="shift" class="form-control" id="shift" placeholder="Enter Shift" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" data-dismiss="shift">Shift Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
