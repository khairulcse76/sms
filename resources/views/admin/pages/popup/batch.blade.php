<div class="modal fade" tabindex="-1" role="dialog" id="batch-show">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Create New Batch
            </div>
            <form action="{{URL('authorize/insert-batch')}}" method="POST" id="frm-batch-create">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="batch" class="control-label">Batch:</label>
                        <input name="batch" class="form-control" id="batch" placeholder="Enter Batch Name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" data-dismiss="">Create Batch</button>
                </div>
            </form>
        </div>
    </div>
</div>
