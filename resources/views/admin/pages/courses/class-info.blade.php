@extends('admin.layouts.admin-master')
@section('title') SMS || Manage Course @endsection
@section('page_header') Manage Course @endsection


@section('style')
    <link rel="stylesheet" href="{{asset('/admin-panel/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('main_content')
    <div class="row">
        <div class="col-lg-12 col-lg-offset-0">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Program</th>
                            <th>Level</th>
                            <th>Shift</th>
                            <th>Time</th>
                            <th>Academics Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($classes as $value)
                        <tr>
                            <td>{{ $value->program }}</td>
                            <td>{{ $value->level }}</td>
                            <td>{{ $value->shift }}</td>
                            <td>{{ $value->time }}</td>
                            <td>
                                <a href="#" data-id="{{ $value->class_id }}">
                                    Program: {{ $value->program }}/ Level: {{$value->level}}/ Shift: {{ $value->shift }}/ Time: {{ $value->time }}/ Batch: {{$value->batch}}
                                    / Groups: {{ $value->group }}/ Start Date: {{ date('d/m/Y'), strtotime($value->start_date) }}/ End Date: {{ date('d/m/Y'), strtotime($value->end_date) }}
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th>Engine version</th>
                            <th>CSS grade</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('admin-panel/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-panel/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
