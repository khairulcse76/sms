@extends('admin.layouts.admin-master')
@section('title') SMS || Manage Course @endsection
@section('page_header') Manage Course @endsection


@section('style')
    <link rel="stylesheet" href="{{asset('/admin-panel/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('main_content')

    @include('admin.pages.popup.academic')
    @include('admin.pages.popup.program')
    @include('admin.pages.popup.level')
    @include('admin.pages.popup.shift')
    @include('admin.pages.popup.time')
    @include('admin.pages.popup.batch')
    @include('admin.pages.popup.groups')
    @include('admin.pages.popup.errormas')
    @include('admin.pages.popup.success')

    <div class="row">
        <div class="col-lg-12 col-lg-offset-0">
            <section class="panel panel-default">
                <div class="panel-heading">Manage Course</div>
                <form action="{{ url('authorize/create-new-course') }}" method="post" class="form-horizontal myclass" id="frm-course-create">
                    {{ csrf_field() }}
                    <div class="panel-body">
                    {{--//===========================//--}}
                        <div class="col-sm-3">
                            <label>Academic Year</label>
                            <div class="input-group">
                                <div id="reload">
                                    <select class="form-control" name="academic_id" id="academic_id">
                                        @foreach($academic as $value)
                                            <option value="{{ $value->academic_id }}">{{ $value->academic }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group-addon" id="add-more-academic">
                                    <span class="fa fa-plus"></span>
                                </div>
                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-3">
                            <label for="program">Course</label>
                            <div class="input-group">
                                <div class="preload">
                                    <select class="form-control" name="program_id" id="program_id">
                                        <option value="">Select Course</option>
                                        @foreach($programs as $value)
                                            <option value="{{ $value->program_id }}">{{ $value->program }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group-addon" id="add-more-program">
                                    <span class="fa fa-plus"></span>
                                </div>

                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-5">
                            <label for="level">Level</label>
                            <div class="input-group">
                                <select class="form-control" name="level_id" id="level_id">
                                    <option value="">please select Coures</option>
                                </select>
                                <div class="input-group-addon"  id="add-more-level">
                                    <span class="fa fa-plus"></span>
                                </div>
                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-3">
                            <label for="shift">Shift</label>
                            <div class="input-group">
                                <div id="sreload">
                                    <select class="form-control" name="shift_id" id="shift_id">
                                        <option value="">Select Shift</option>
                                        @foreach($shifts as $value)
                                            <option value="{{ $value->shift_id }}">{{ $value->shift }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group-addon" id="create-more-shift">
                                    <span class="fa fa-plus"></span>
                                </div>

                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-3">
                            <label for="time">Time</label>
                            <div class="input-group">
                                <div id="treload">
                                    <select class="form-control" name="time_id" id="time_id">
                                        <option value="">Select Time</option>
                                        @foreach($times as $value)
                                            <option value="{{ $value->time_id }}">{{ $value->time }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group-addon" id="add-more-time">
                                    <span class="fa fa-calendar"></span>
                                </div>

                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-3">
                            <label for="batch">Batch</label>
                            <div class="input-group">
                                <div id="batchreload">
                                    <select class="form-control" name="batch_id" id="batch_id">
                                        <option value="">Select Batch</option>
                                        @foreach($batch as $value)
                                            <option value="{{ $value->batch_id }}">{{ $value->batch }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group-addon" id="add-more-batch">
                                    <span class="fa fa-plus"></span>
                                </div>

                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-2">
                            <label for="group">Group</label>
                            <div class="input-group">
                                <div id="groupreload">
                                    <select class="form-control" name="group_id" id="group_id">
                                        <option value="">Select</option>
                                        @foreach($groups as $value)
                                            <option value="{{ $value->group_id }}">{{ $value->group }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group-addon" id="add-more-group">
                                    <span class="fa fa-plus"></span>
                                </div>

                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-4">
                            <label for="Start-date">Start Date</label>
                            <div class="input-group">
                                <input class="form-control" name="start_date" id="start_date" placeholder="Start date">
                                <div class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </div>
                            </div>
                        </div>
                        {{--===========================--}}
                        <div class="col-sm-4">
                            <label for="End-date">End Date</label>
                            <div class="input-group">
                                <input class="form-control" name="end_date" id="end_date" placeholder="End date">
                                <div class="input-group-addon" id="end_date">
                                    <span class="fa fa-calendar"></span>
                                </div>
                            </div>
                        </div>
                        {{--===========================--}}
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Create Course</button>
                    </div>
                </form>
            </section>
        </div>
        {{ csrf_field() }}
    </div>


    <?php
    $classes = \App\MyClass::join('academics', 'academics.academic_id', '=', 'my_classes.academic_id')
        ->join('levels', 'levels.level_id', '=', 'my_classes.level_id')
        ->join('programs', 'programs.program_id', '=', 'levels.level_id')
        ->join('shifts', 'shifts.shift_id', '=', 'my_classes.shift_id')
        ->join('times', 'times.time_id', '=', 'my_classes.time_id')
        ->join('groups', 'groups.group_id', '=', 'my_classes.group_id')
        ->join('batches', 'batches.batch_id', '=', 'my_classes.batch_id')
        ->orderBy('my_classes.class_id', 'desc')
        ->get();

    ?>

    <div class="row">
        <div class="col-lg-12 col-lg-offset-0">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Class information</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="add_data_info">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Program</th>
                            <th>Level</th>
                            <th>Shift</th>
                            <th>Time</th>
                            <th>Academics Details</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $id=1; ?>
                        @foreach($classes as $value)
                            <tr>
                                <td><?php echo $id++ ?></td>
                                <td>{{ $value->program }}</td>
                                <td>{{ $value->level }}</td>
                                <td>{{ $value->shift }}</td>
                                <td>{{ $value->time }}</td>
                                <td>
                                    <a href="#" data-id="{{ $value->class_id }}">
                                        Program: {{ $value->program }}/ Level: {{$value->level}}/ Shift: {{ $value->shift }}/ Time: {{ $value->time }}/ Batch: {{$value->batch}}
                                        / Groups: {{ $value->group }}/ Start Date: {{ date('d/m/Y'), strtotime($value->start_date) }}/ End Date: {{ date('d/m/Y'), strtotime($value->end_date) }}
                                    </a>
                                </td>
                                <td><a href="{{ $value->class_id }}"><span class="fa fa-trash" style="font-size: 24px; color:red"></span></a> </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('admin-panel/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-panel/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    @include('admin.pages.script.course-script')
@endsection
