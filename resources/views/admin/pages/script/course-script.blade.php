<script type="text/javascript">
    $('#start_date').datepicker();
    $('#end_date').datepicker();

    //================================================================//
    //Course Create script
    //================================================================//

        $('#frm-course-create').on('submit', function (e) {
            e.preventDefault();
            var data=$(this).serialize();
            var url=$(this).attr('action');
            $.ajax({
                url: $(this).attr('action'),
                type:'POST',
                data: $(this).serialize(),
                success: function(data) {
                    if($.isEmptyObject(data.error)){
                        $(this).trigger('reset');
                        $('#success-msg-show').modal();
                    }else{
                        printErrorMsg(data.error);
                    }
                }
            });
            function printErrorMsg (msg) {
                $('#errmsg-show').modal();
                $(".print-error-msg").find("ul").html('');
                    $(".print-error-msg").css('display','block');
                    $.each( msg, function( key, value ) {
                        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                });
            }
        });
    //================================================================//
     //             data information
    // ================================================================//


    // ================================================================//
    //Group script
    //================================================================//
    $('#add-more-group').on('click', function () {
        $('#group-show').modal();
        $('#frm-group-create').on('submit', function (e) {
            e.preventDefault();
            var data=$(this).serialize();
            var url=$(this).attr('action');
            $.post(url, data, function (success) {
                $('#groupreload').load(location + ' #groupreload');
                    console.log(success);
            });
            $(this).trigger('reset');
        })
    });
    //================================================================//
    //Batch script
    //================================================================//
    $('#add-more-batch').on('click', function () {
        $('#batch-show').modal();

        $('#frm-batch-create').on('submit', function (e) {
            e.preventDefault();
            var data=$(this).serialize();
            var url=$(this).attr('action');
            $.post(url, data, function (success) {
                $('#batchreload').load(location + ' #batchreload');
                    console.log(success);
            });
            $(this).trigger('reset');
        })
    });
    //================================================================//
    //Time script
    //================================================================//
    $('#add-more-time').on('click', function () {
        $('#time-show').modal();

        $('#frm-time-create').on('submit', function (e) {
            e.preventDefault();
            var data=$(this).serialize();
            var url=$(this).attr('action');
            $.post(url, data, function (success) {
                $('#treload').load(location + ' #treload');
                    console.log(success);
            });
            $(this).trigger('reset');
        })
    });

    //================================================================//
    //Shift Function
    //================================================================//

    $('#create-more-shift').on('click', function () {
        $('#shift-show').modal();
        $('#frm-shift-create').on('submit', function (e) {
            e.preventDefault();
            var data=$(this).serialize();
            var url=$(this).attr('action');
            $.post(url, data, function (success) {
                $('#sreload').load(location + ' #sreload');
                console.log(success);
            });
            $(this).trigger('reset');
        });
    });

    //================================================================//
    //End shift Function
    //================================================================//
    $('#add-more-academic').on('click', function () {
        $('#academic-year-show').modal();
    });
    $('.btn-save-academic').on('click', function () {
        var academic = $('#new-academic').val()
        $.post('insert-academic-year', { 'academic': academic, '_token':$('input[name=_token]').val()}, function (data) {
            $('#reload').load(location + ' #reload');
            $('#academic_id').append($("<option/>", {
                value:data.academic_id,
                text:data.academic
            }));
        });
        $(this).trigger('reset');
    });

    //================================================================//
    $('#add-more-program').on('click', function () {
        $('#program-show').modal();
    });
    $('.btn-save-program').on('click', function () {
        var program = $('#program').val();
        var description = $('#description').val();
        $.post('insert-program', { 'description': description, 'program':program, '_token':$('input[name=_token]').val()}, function (data) {
            $('.preload').load(location + ' .preload');
        });
        $(this).trigger('reset');
    });

    //================================================================//

    //================================================================//
    $('#add-more-level').on('click', function (){
        var programs=$('#program_id option');
        var program=$('#frm-level-create').find('#program_id');
        $(program).empty();
        $.each(programs, function (i,pro) {
            $(program).append($("<option/>", {
                value:$(pro).val(),
                text:$(pro).text()
            }));
        });
        $('#level-show').modal();
    });
    $('#frm-level-create').on('submit', function (e) {
        e.preventDefault();
        var data=$(this).serialize();
        var url=$(this).attr('action');
        $.post(url, data, function (result) {
            $('#level_id').append($("<option/>", {
                value : result.level_id,
                text : result.level
            }));
            console.log(result);
        });
        $(this).trigger('reset');
    });

    $('#frm-course-create #program_id').on('change', function (e) {
        var program_id=$(this).val();
        var levelvalue=$('#level_id');
        $(levelvalue).empty();
        $.get('/authorize/show-level',{'id':program_id}, function (data) {
            $.each(data, function (i, level) {
                $(levelvalue).append($("<option/>", {
                    value : level.level_id,
                    text : level.level
                }));
            });
            console.log(data);
        });
    });
    // ===============================  //

</script>