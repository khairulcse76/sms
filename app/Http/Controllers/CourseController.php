<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Group;
use App\Level;
use App\MyClass;
use App\Program;
use App\Shift;
use App\Time;
use Illuminate\Http\Request;
use App\Academic;
use DB;
use Validator;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $academic = Academic::OrderBy('academic_id', 'desc')->get();
        $programs = Program::OrderBy('program_id', 'desc')->get();
        $shifts = Shift::OrderBy('shift_id', 'desc')->get();
        $times = Time::OrderBy('time_id', 'desc')->get();
        $batch = Batch::OrderBy('batch_id', 'desc')->get();
        $groups = Group::OrderBy('group_id', 'desc')->get();
        return view('admin.pages.courses.manage-course', compact('academic', 'programs', 'shifts', 'times', 'batch', 'groups'));
    }

    public function createcourse(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'academic_id' => 'required',
            'level_id' => 'required',
            'shift_id' => 'required',
            'time_id' => 'required',
            'group_id' => 'required',
            'batch_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        if ($validator->passes()) {
            $courses = MyClass::create($request->all());
            return response()->json(['success' => 'Course Successfully Create']);
        } else {
            return response()->json(['error' => $validator->errors()->all()]);
        }


//        if ($request->ajax()){
//            $courses= MyClass::create($request->all());
//            return response('success');
//        }

    }

    public function insert_academic_year(Request $request)
    {
        if ($request->ajax()) {
            $academic = Academic::create($request->all());
            return response($academic);
        }
    }

    public function insert_program(Request $request)
    {
        if ($request->ajax()) {
            $programs = Program::create($request->all());
            return response($programs);
        }
    }

    public function insert_level(Request $request)
    {
        if ($request->ajax()) {
            $levels = Level::create($request->all());
            return response($levels);
        }
    }

    public function show_level(Request $request)
    {
        if ($request->ajax()) {
            $reseult = Level::where('program_id', $request->id)->get();

            return response($reseult);
        }
    }

    public function insert_shift(Request $request)
    {
        if ($request->ajax()) {
            $shift = Shift::create($request->all());
            return response($shift);
        }
    }

    public function insert_batch(Request $request)
    {
        if ($request->ajax()) {
            $batch = Batch::create($request->all());
            return response('Batch Insert Successful');
        }
    }

    public function insert_group(Request $request)
    {
        if ($request->ajax()) {
            $group = Group::create($request->all());
            return response('Group Insert Successful' . $group);
        }
    }

    public function insert_time(Request $request)
    {
        if ($request->ajax()) {
            $arr = array($request->start_time, $request->end_time);
            $time = implode(" to ", $arr);
            $times = new Time;
            $times->time = $time;
            $times->save();
            return response('success');
        }
    }


    public function show_class_info(Request $request)
    {
        return response($this->class_info()->get());

    }
    public function class_info()
    {
        $classes = MyClass::join('academics', 'academics.academic_id', '=', 'my_classes.academic_id')
//            ->join('programs', 'programs.program_id', '=', 'my_classes.level_id')
            ->join('levels', 'levels.level_id', '=', 'my_classes.level_id')
            ->join('shifts', 'shifts.shift_id', '=', 'my_classes.shift_id')
            ->join('times', 'times.time_id', '=', 'my_classes.time_id')
            ->join('groups', 'groups.group_id', '=', 'my_classes.group_id')
            ->join('batches', 'batches.batch_id', '=', 'my_classes.batch_id');


        return view('admin.pages.courses.class-info', compact('classes'));

    }




}

