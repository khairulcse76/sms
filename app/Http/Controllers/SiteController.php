<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SiteController extends Controller
{
    public function welcome(){

        DB::table('site_informations')->increment('visitors');

        return view('welcome');
    }
}
