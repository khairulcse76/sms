<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyClass extends Model
{
    protected $fillable = [
        'academic_id',
        'level_id',
        'shift_id',
        'time_id',
        'group_id',
        'batch_id',
        'start_date',
        'start_date',
        'end_date',
        'active',
    ];
//    protected $dateFormat = 'd/m/Y';
}
