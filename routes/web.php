<?php

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'SiteController@welcome');

Route::get('/admin', 'LoginController@create');


//===========================//
Route::group([
    'prefix' => Config("authorization.route-prefix"),
    'middleware' =>  ['authorize', 'auth']
],
    function() {
        Route::get('/dashboard', 'AdminController@index');
        Route::get('/manage-course', 'CourseController@index');
        Route::post('/create-new-course', 'CourseController@createcourse');
        Route::post('/insert-academic-year', 'CourseController@insert_academic_year');
        Route::post('/insert-program', 'CourseController@insert_program');
        Route::post('/insert-level', 'CourseController@insert_level');
        Route::get('/show-level', 'CourseController@show_level');
        Route::post('/insert-shift', 'CourseController@insert_shift');
        Route::post('/insert-time', 'CourseController@insert_time');
        Route::post('/insert-batch', 'CourseController@insert_batch');
        Route::post('/insert-group', 'CourseController@insert_group');
        Route::get('/courses/class/info', 'CourseController@show_class_info');
        });
//==============================//

Route::group([
    'prefix' => Config("authorization.route-prefix"),
    'middleware' => ['web', 'auth']],
    function() {
        Route::group(['middleware' => Config("authorization.middleware")], function() {
            Route::resource('users', 'UsersController', ['except' => [
                'create', 'store', 'show'
            ]]);
            Route::resource('roles', 'RolesController');
            Route::get('/permissions', 'PermissionsController@index');
            Route::post('/permissions', 'PermissionsController@update');
            Route::post('/permissions/getSelectedRoutes', 'PermissionsController@getSelectedRoutes');
        });

        Route::get('/', function () {
            return view('vendor.authorize.welcome');
        });
    });