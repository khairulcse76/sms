<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('student_id');
            $table->string('first_name', 20)->nullable();
            $table->string('last_name',20)->nullable();
            $table->tinyInteger('sex')->nullable();
            $table->date('dob')->nullable();
            $table->string('email')->nullable();
            $table->string('rac')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('nationality', 50)->nullable();
            $table->string('national_card',50)->nullable();
            $table->string('passport',50)->nullable();
            $table->string('phone',50)->nullable();
            $table->string('village',50)->nullable();
            $table->string('commune',50)->nullable();
            $table->string('district',50)->nullable();
            $table->string('province',50)->nullable();
            $table->string('current_address',100)->nullable();
            $table->date('date_register',50)->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('photo',50)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
