<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Site_InformationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $site_info = [
            [
                'visitors' => '0',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

        ];
        DB::table('site_informations')->insert($site_info);
    }
}
